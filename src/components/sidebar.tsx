import { valueToPercent } from '@mui/base'
import React from 'react'
import { SidebarData } from './SidebarData'
import SidebarIcon from './SidebarIcon'

const Sidebar = () => {

    return (
        <div className="Sidebar">
            <SidebarIcon />
            <ul className="SidebarList">
                {SidebarData.map((data, key) => (
                    <li key={key} className="row" onClick={()=>window.location.pathname=data.link}
                        id={window.location.pathname== data.link?"active":""}>
                        <div id="icon">{data.icon}</div>
                        <div id="title">{data.title}</div>            
                    </li>
                ))}
            </ul>
        </div>
    )
}

export default Sidebar